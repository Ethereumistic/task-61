import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {

const hotPrices = document.querySelectorAll('.hot');

for (const hotPrice of hotPrices) {
  const emoji = document.createElement('span');
  emoji.textContent = "🔥";
  hotPrice.append(emoji);
}
});
